import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DesafioFabrica {
    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver", "C:/driver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    }
    @After
    public void sair() {driver.quit();}

    @Test
    public void testarCamposPreenchidos() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[1]/td/input")).sendKeys("larissa");
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[2]/td/input")).sendKeys("123456");
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[3]/td/textarea")).sendKeys("test");
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[1]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[2]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Processed Form Details", driver.findElement(By.xpath("/html/body/div/h1")).getText());
    }
    @Test
    public void testarCamposIncompletos() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[2]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[4]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();
        Thread.sleep(3000);
        Assert.assertEquals("No Value for username", driver.findElement(By.xpath("/html/body/div/div[3]/p[1]/strong")).getText());
    }
    @Test
    public void testarUserNameNumeric() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[1]/td/input")).sendKeys("123456");
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[1]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[2]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();
        Thread.sleep(3000);
        Assert.assertEquals("123456", driver.findElement(By.id("_valueusername")).getText());
    }
}

